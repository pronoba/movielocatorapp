//
//  SKMovieModel.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 11/30/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import UIKit
import CoreLocation

public class SKMovieModel: NSObject {

    var movieName: String?
    var releaseYear: String?
    var movieLocation: String?
    var coordinates: CLLocation?
    
    override init() {
        
        super.init()
    }
    
    convenience required public init?(jsonData: [AnyObject], columnFormat: [String]) {

        self.init()
        
        movieName = jsonData[columnFormat.index(of: "title")!] as? String ?? ""
        movieLocation = jsonData[columnFormat.index(of: "locations")!] as? String ?? ""
        releaseYear = jsonData[columnFormat.index(of: "release_year")!] as? String  ?? ""
    }
    
}
