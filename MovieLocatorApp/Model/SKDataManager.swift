//
//  SKDataManager.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 11/30/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import UIKit

class SKDataManager: NSObject {
    
    lazy var movieList = [SKMovieModel]()

    static let sharedInstance = SKDataManager()
    
    private override  init() {}

}
