//
//  SKMapAnnotation.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 12/3/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import UIKit
import MapKit

class SKMapAnnotation: NSObject, MKAnnotation {
    
    var movieDetails: Movie?
    
    var movieLocation: String?
    var title: String?
    var coordinate: CLLocationCoordinate2D
    
    
    init(_ movie: Movie) {
        
        movieDetails = movie
        coordinate = CLLocationCoordinate2DMake(0.0, 0.0)

        if let movieName = movie.movieName {

            title = movieName
            
        }

        if let address = movie.movieLocation {

            movieLocation = address
            
        }
        
        super.init()
    }

}
