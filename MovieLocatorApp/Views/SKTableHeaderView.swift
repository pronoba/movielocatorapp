//
//  SKTableHeaderView.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 12/4/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import UIKit

public protocol SKTableHeaderViewDelegate {
    func movieNameButtonPressed()
    func locationButtonPressed()
    func releaseYearButtonPressed()
}

class SKTableHeaderView: UIView {

    open var delegate: SKTableHeaderViewDelegate?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBAction func movieNamePressed(_ sender: Any) {
        
        delegate?.movieNameButtonPressed()
    }
    
    @IBAction func locationPressed(_ sender: Any) {
        
        delegate?.locationButtonPressed()
    }
    
    @IBAction func releaseYearPressed(_ sender: Any) {
        
        delegate?.releaseYearButtonPressed()
    }
    
}
