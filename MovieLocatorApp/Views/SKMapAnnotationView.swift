//
//  SKMapAnnotationView.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 12/3/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import UIKit
import MapKit

class SKMapAnnotationView: MKAnnotationView {

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        self.isDraggable = false
        self.backgroundColor = UIColor.clear
        self.canShowCallout = true
        self.image = UIImage(named: "mapPin")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
