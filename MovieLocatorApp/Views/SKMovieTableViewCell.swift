//
//  SKMovieTableViewCell.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 12/3/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import UIKit

class SKMovieTableViewCell: UITableViewCell {

    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(movie: Movie) {
        movieNameLabel.text = movie.movieName
        locationLabel.text = movie.movieLocation
        dateLabel.text = movie.releaseYear
    }


}
