//
//  SKMovieDataSource.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 12/3/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import UIKit
import CoreData

class SKMovieDataSource: NSObject {

    public typealias SKModelFailureHandler = (Error) -> Void

    
    func getMovieData(successHandler: @escaping () -> Void, failureHandler: @escaping SKModelFailureHandler ) {

        _ = SKWebServices.sharedInstance.requestMovieEnpoint(successHandler: { [weak self] movieModelJSON in

            self?.parseJSON(jsonData: movieModelJSON)
            
            successHandler()
            
        }) { (error) in
            
            print("\(error)")
            failureHandler(error)
            
        }

    }
    
    
    private func deleteAllEntries() {
        do {
            
            let context = SKCoreDataStack.sharedInstance.persistentContainer.viewContext
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Movie")

            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                
                _ = objects.map{$0.map{context.delete($0)}}
                
                SKCoreDataStack.sharedInstance.saveContext()
                
            } catch let error {
                print("Error: \(error)")
            }
        }
    }
    
    func parseJSON(jsonData: [String: AnyObject]) {
        
        let columnsJSON = (jsonData["meta"]!["view"] as! [String: AnyObject]) ["columns"] as! [[String: AnyObject]]
        
        var columnFormat = [String]()
        
        for columnItem in columnsJSON {
            
            columnFormat.append(columnItem["fieldName"] as! String)
        }
        
        let data = jsonData["data"] as! [[AnyObject]]
        
        
        self.deleteAllEntries()
        
        for dataItem in data {
            

            let context = SKCoreDataStack.sharedInstance.persistentContainer.viewContext
            
            if let movieEntity = NSEntityDescription.insertNewObject(forEntityName: "Movie", into: context) as? Movie {

                movieEntity.movieName = dataItem[columnFormat.index(of: "title")!] as? String ?? ""
                movieEntity.releaseYear = dataItem[columnFormat.index(of: "release_year")!] as? String ?? ""
                movieEntity.movieLocation = dataItem[columnFormat.index(of: "locations")!] as? String  ?? ""
                
            }
        }

        
        do {
            
            try SKCoreDataStack.sharedInstance.persistentContainer.viewContext.save()
            
        } catch let error {
            
            print(error)
        }

    }

}
