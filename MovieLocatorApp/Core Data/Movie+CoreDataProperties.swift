//
//  Movie+CoreDataProperties.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 12/3/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        return NSFetchRequest<Movie>(entityName: "Movie")
    }

    @NSManaged public var movieLocation: String?
    @NSManaged public var movieName: String?
    @NSManaged public var releaseYear: String?
    @NSManaged public var movieUUID: NSObject?

}
