//
//  DetailViewController.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 11/30/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController, MKMapViewDelegate  {
    
    let LATITUDE_SPAN = 69.0
    let LONGITUDE_SPAN = 69.0
    let defaultMapRadius = 10.0
    
    lazy var geoCoder = CLGeocoder()
    var movieAnnotations = [SKMapAnnotation]()
    private var userLocation: MKUserLocation?
    let locationManager = CLLocationManager()
    
    lazy var sanFranCoordinates = CLLocationCoordinate2DMake(37.773972, -122.431297)

    var movieDetails: Movie?

    // MARK: - GETTERS
    
    var latitudeSpanRadius: Double {
        get {
            
            return defaultMapRadius / LATITUDE_SPAN
        }
    }

    var longitudeSpanRadius: Double {
        get {
            
            return defaultMapRadius / LONGITUDE_SPAN
        }
    }

    // MARK: - IBOutlets
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Section

    func configureView() {
        
        // Update the user interface for the detail item.
        if let movieDetail = movieDetails {
            
            guard let movieLocation = movieDetail.movieLocation, movieLocation != "" else { return }
            
            self.activityIndicator.startAnimating()

            // reverse geocode an address to a location coordinate
            geoCoder.geocodeAddressString(movieLocation + "San Francisco, CA", completionHandler: { [weak self] (placeMarks, error) in

                guard let strongSelf = self else { return }
                
                DispatchQueue.main.async {
                    
                    strongSelf.activityIndicator.stopAnimating()
                }
                
                guard placeMarks != nil, placeMarks?.count != 0 else { return }
                
                DispatchQueue.main.async {
                    for placeMark in placeMarks! {
                        
                        strongSelf.movieAnnotations.removeAll()
                        
                        let movieAnnotation = SKMapAnnotation(strongSelf.movieDetails!)
                        movieAnnotation.coordinate = (placeMark.location?.coordinate)!
                        strongSelf.movieAnnotations.append(movieAnnotation)
                        // no need for multple
                        break
                    }
                    strongSelf.reloadAnnotations()
                    strongSelf.zoomMapViewToShowAnnotations()
                }
            })
            
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupMap()
        requestLocationAccess()
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupMap() {

        mapView.delegate = self
        mapView.userTrackingMode = .none
        mapView.showsPointsOfInterest = false
        mapView.showsUserLocation = true
                
        // create a region for San Francisco - default

        // set the map to SF
        let span = MKCoordinateSpanMake(CLLocationDegrees(latitudeSpanRadius), CLLocationDegrees(longitudeSpanRadius))
        let region = MKCoordinateRegionMake(sanFranCoordinates, span)
        mapView.setRegion(region, animated: false)
        
    }
    
    func reloadAnnotations() {
        if mapView.annotations.count > 0 {
            self.mapView.annotations.forEach {
                if !($0 is MKUserLocation) {
                    self.mapView.removeAnnotation($0)
                }
            }
        }
        
        self.mapView.addAnnotations(movieAnnotations)
    }
    
    
    func requestLocationAccess() {
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return
            
        case .denied, .restricted:
            print("location access denied")
            
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func zoomMapViewToShowAnnotations() {
        guard !movieAnnotations.isEmpty else { return }
        
        let currentLocation =  userLocation?.location  ?? CLLocation(latitude: sanFranCoordinates.latitude, longitude: sanFranCoordinates.longitude)
        
        // 1. Calculate how far each annotation is from user location and store in an array
        // 3. Sort the array
        // 4. Pick up the farest distance according to the needed number of annotations to be displayed
        // 5. Calculate map region according to the distance
        // 6. Zoom to the region
        
        var distances = [CLLocationDistance]()
        
        var mainMovieLocation: CLLocation?
        
        for annotation in movieAnnotations {
            let movieLocation = CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude)
            
            if mainMovieLocation == nil {
                mainMovieLocation = movieLocation
            }
            
            let distance = currentLocation.distance(from: movieLocation)
            distances.append(distance)
        }
        
        distances.sort { $0 < $1 }
        
        let numberOfAnnotationsToShow = Int(defaultMapRadius)
        var farestDistance: CLLocationDistance = 0
        if numberOfAnnotationsToShow >= distances.count {
            farestDistance = distances.last!
        } else {
            farestDistance = distances[numberOfAnnotationsToShow-1]
        }
        
        farestDistance = farestDistance < 250.0 ? 250.0 : farestDistance
        
        let center = mainMovieLocation?.coordinate ?? currentLocation.coordinate
        // This method takes north-to-south distance, so multiplied by 2. The extra 0.4 is used for setting the map padding, so that the movie icon doesn't get cut-off
        
        var region = MKCoordinateRegionMakeWithDistance(center, 2.4 * farestDistance, 2.4 * farestDistance)
        
        // There is a boundary of span, if cross it setRegion will throw exception
        if region.span.latitudeDelta >= 180 || region.span.longitudeDelta >= 360 {
            let span = MKCoordinateSpan(latitudeDelta: 179, longitudeDelta: 359)
            region.span = span
        }
        
        mapView.setRegion(region, animated: true)
    }
    
    // MARK: - CLLocationManagerDelegate
    
    public func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        self.userLocation = userLocation
        if let userLocationView = mapView.view(for: mapView.userLocation) {
            userLocationView.superview!.bringSubview(toFront: userLocationView)
        }
    }
    
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var mapAnnotationView: SKMapAnnotationView
        
        if(annotation is MKUserLocation) {
            return nil
        }
        
        if annotation is SKMapAnnotation {
            if let annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "moviePin") as? SKMapAnnotationView {
                let mapAnnotation = annotation as! SKMapAnnotation
                annotationView.annotation = mapAnnotation
                mapAnnotationView = annotationView
            } else {
                mapAnnotationView = SKMapAnnotationView(annotation: annotation, reuseIdentifier: "moviePin")
            }
            return mapAnnotationView
        }
        
        return nil
    }
    
}

