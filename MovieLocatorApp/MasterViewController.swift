//
//  MasterViewController.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 11/30/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UITableViewController, NSFetchedResultsControllerDelegate, SKTableHeaderViewDelegate {

    var detailViewController: DetailViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    
    var currentSortDescriptor = ("movieName", true)
    
    lazy var movieDataSource = SKMovieDataSource()


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.configureTableView()

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }

        tableView.register(UINib.init(nibName: "SKTableHeaderView", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "skTableHeaderView")

        self.tableView.isUserInteractionEnabled = false
        
        _ = movieDataSource.getMovieData(successHandler: { [weak self] in
            self?.tableView.isUserInteractionEnabled = true
        }, failureHandler: { [weak self]  error in
            self?.tableView.isUserInteractionEnabled = true
            print("\(error)")
        })
        
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func configureTableView() {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100.0
    }

    @objc

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
            let object = fetchedResultsController.object(at: indexPath)
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.movieDetails = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! SKMovieTableViewCell
        let movie = fetchedResultsController.object(at: indexPath)
        cell.configureCell(movie: movie)
        return cell
    }


    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = Bundle.main.loadNibNamed("SKTableHeaderView", owner: self, options: nil)
        let sectionHeaderView = nib?.first as? SKTableHeaderView
        sectionHeaderView?.delegate = self
        
        return sectionHeaderView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    
    // MARK: - Fetched results controller

    var fetchedResultsController: NSFetchedResultsController<Movie> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: currentSortDescriptor.0, ascending: currentSortDescriptor.1)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Master")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
             let nserror = error as NSError
             fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }    
    var _fetchedResultsController: NSFetchedResultsController<Movie>? = nil

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
            case .insert:
                tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
            case .delete:
                tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
            default:
                return
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
            case .insert:
                tableView.insertRows(at: [newIndexPath!], with: .fade)
            case .delete:
                tableView.deleteRows(at: [indexPath!], with: .fade)
            case .update:
                let cell = tableView.cellForRow(at: indexPath!)! as! SKMovieTableViewCell
                cell.configureCell(movie: anObject as! Movie)
            case .move:
                let cell = tableView.cellForRow(at: indexPath!)! as! SKMovieTableViewCell
                cell.configureCell(movie: anObject as! Movie)
                tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    func requeryFetchedResultsController() {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: "Master")
        let sortDescriptor = NSSortDescriptor(key: currentSortDescriptor.0, ascending: currentSortDescriptor.1)
        self.fetchedResultsController.fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        self.tableView.reloadData()
    }
    
    // MARK :- SKTableHeaderViewDelegate
    
    func movieNameButtonPressed() {

        currentSortDescriptor = ("movieName", !currentSortDescriptor.1)
        self.requeryFetchedResultsController()

    }
    
    func locationButtonPressed() {
        
        currentSortDescriptor = ("movieLocation", !currentSortDescriptor.1)
        self.requeryFetchedResultsController()

    }
    
    func releaseYearButtonPressed() {
        
        currentSortDescriptor = ("releaseYear", !currentSortDescriptor.1)
        self.requeryFetchedResultsController()

    }


}

