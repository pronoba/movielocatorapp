//
//  SKWebServices+Endpoint.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 11/30/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import Foundation
import UIKit

extension SKWebServices {

    public typealias SKMovieSuccessHandler = ([String: AnyObject]) -> Void
    public typealias SKMovieFailureHandler = (Error) -> Void

    // get the data from the
    
    func requestMovieEnpoint(successHandler: @escaping SKMovieSuccessHandler, failureHandler: @escaping SKMovieFailureHandler ) {
        
        let endpointURLString = baseURL + endPoint + query;
        
        self.getEndpoint(endPointURL: endpointURLString,
                         successHandler: { jsonData in
            
            successHandler(jsonData)
            
        }) { error in
            
            // handle error
            failureHandler(error!)

        }
    }

}
