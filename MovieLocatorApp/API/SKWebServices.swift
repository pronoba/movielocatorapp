//
//  SKWebServices.swift
//  MovieLocatorApp
//
//  Created by Pronob Ashwin on 11/30/17.
//  Copyright © 2017 Pronob Ashwin. All rights reserved.
//

import Foundation
import UIKit


public final class SKWebServices: NSObject {

    public typealias SKSuccessHandler = ([String: AnyObject]) -> Void
    public typealias SKFailureHandler = (_ error: Error?) -> Void

    static let sharedInstance = SKWebServices()
    
    private override  init() {}

    public func getEndpoint(endPointURL: String,
                     successHandler: @escaping SKSuccessHandler,
                     failureHandler: @escaping SKFailureHandler) {

        guard let url = URL(string: endPointURL)
            else {
                print("Error: URL string is malformed")
                return
        }
        
        let sharedSession = URLSession.shared
        
        let task = sharedSession.dataTask(with: url) { (urlResponseData, response, error) in
            
            guard error == nil else {

                print(error!) // Handle error
                return
            }
            
            guard let urlResponseData = urlResponseData else
            {

                print("Error: URL data is malformed") // Handle error
                return
            }
            
            do {
                
                // extract the data and convert it to JSON format before returning a mutable array (.mutableContainers)
                guard let json = try JSONSerialization.jsonObject(with: urlResponseData, options: [.mutableContainers]) as? [String: AnyObject] else {
                    print("Error: JSONSerialization error")
                    return
                }
                
                
                DispatchQueue.main.async {
                    successHandler(json)
                }
                
            } catch let error {
                
                print("Error: \(error)")
                failureHandler(error)
                
            }
            
        }
        
        task.resume()
       
    }
}
